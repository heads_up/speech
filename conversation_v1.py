import json
from watson_developer_cloud import ConversationV1
import sys

def integrate_with_watson(transcribed_text):
    print("entering watson....")
    print("reading text as:" + transcribed_text)
    conversation = ConversationV1(
        username='01d5b944-f5fe-4f37-ae22-360077bc0c2d',
        password='KjpySppoGMDZ',
        version='2016-09-20')

    # replace with your own workspace_id
    workspace_id = 'b9217e6f-6a6a-45e8-9ade-191561583a06'

    response = conversation.message(workspace_id=workspace_id, message_input={'text':transcribed_text})
    print(json.dumps(response, indent=2))

    # When you send multiple requests for the same conversation, include the context object from the previous response.
    response = conversation.message(workspace_id=workspace_id, message_input={'text': 'yes'},
                                    context=response['context'])
    print(json.dumps(response, indent=2))
