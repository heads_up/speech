# README #

### How do I get set up? ###

1. Python 2.7 is required
2. Install Google cloud SDK from https://cloud.google.com/sdk/
	
        run: 
	     gcloud beta auth application-default login

3. Install pip and virtualenv, if you don't already have them.

	      virtaulenv env
	     
              activate the virtual environment 'env' using cmd 
	
              env\scripts\activate
	
              Above command works for windows. However if for you this doesn't work, you can try
	
              env\bin\activate

4. Install following libraries mentioned in requirements.txt file

5. transcribe_streaming_googleasr.py - script is for testing Google Speech API alone. Does not include integration to IBM Watson
   transcribe_streaming_v1.py - script is for testing Google Speech API integrated with IBM Watson

6. To run the script use
	
        python transcribe_streaming_googleasr.py

	 python transcribe_streaming_v1.py